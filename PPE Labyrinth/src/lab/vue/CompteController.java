package lab.vue;

import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import lab.classe.Soignant;
import lab.classe.User;
import lab.controller.ControllerSoignant;
import lab.controller.ControllerUser;
import lab.util.dbConnection;

public class CompteController {
	
	@FXML
	private TextField nom;
	@FXML
	private TextField prenom;
	@FXML
	private PasswordField password;
	
	@FXML
	public void handleConfirmer(ActionEvent event) throws SQLException {
		if(!ControllerSoignant.Soignants.isEmpty()) {
			ControllerSoignant Controller = new ControllerSoignant();
			Controller.Changement(nom.getText().toString(), prenom.getText().toString(), Soignant.hash(password.getText().toString()));
		}
		else if(!ControllerUser.Users.isEmpty()) {
			ControllerUser Controller = new ControllerUser();
			Controller.Changement(nom.getText().toString(), prenom.getText().toString(), User.hash(password.getText().toString()));
		}
		handleCancel(event);
	}
	
	@FXML
	public void handleCancel(ActionEvent event) {
	 ((Node)(event.getSource())).getScene().getWindow().hide();
	}
	
	public void RemplirChamp() {
		if(!ControllerSoignant.Soignants.isEmpty()) {
			Soignant soignant = ControllerSoignant.Soignants.get(0);
			nom.setText(soignant.getNom());
			prenom.setText(soignant.getPrenom());
			password.setText(soignant.getPassword());
			
		}
		 else if(!ControllerUser.Users.isEmpty()) {
				User patient = ControllerUser.Users.get(0);
				nom.setText(patient.getNom());
				prenom.setText(patient.getPrenom());
				password.setText(patient.getPassword());
		}
	}
}
