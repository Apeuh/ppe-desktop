package lab.vue;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.scene.control.*;
import lab.Main;
import lab.controller.ControllerSoignant;
import lab.controller.ControllerUser;
import lab.util.Timeur;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class RootLayoutController implements Initializable {
	
	ObservableList<String> choixList = FXCollections.observableArrayList("Facile","Moyen","Difficile");
	@FXML
	private  ChoiceBox choix;
	@FXML
	private  MenuItem login;
	@FXML
	private MenuItem inscription;
	@FXML
	private  Menu Edit;
	@FXML
	private TextField time;
	@FXML
	private MenuItem deco;
	@FXML
	private MenuItem compte;
	private String path;
	private Main main;
	 public void setMainSisi(Main main) {
	        this.main = main;}
	 @FXML
	 public void handleLogin() {
		    	try {
		    		Stage primaryStage = new Stage();
		            FXMLLoader loader = new FXMLLoader();
		            loader.setLocation(Main.class.getResource("vue/Login.fxml"));
		            Parent page = loader.load();   
		            path="file:C:\\Users\\camil\\eclipse-workspace\\ppe-desktop\\PPE Labyrinth\\src\\assets/Minos.jpg";
		            primaryStage.getIcons().add(new Image(path)); 
		            Scene scene = new Scene(page);
		            primaryStage.setTitle("Login");
		            primaryStage.initModality(Modality.APPLICATION_MODAL);
		            primaryStage.setScene(scene);
		            primaryStage.show();
		        } catch(Exception e) {
		            e.printStackTrace();}
	 }
	 
	 @FXML
		public void handleInscription() {
			try {
	         	Stage primaryStage = new Stage();
	            FXMLLoader loader = new FXMLLoader();
	            loader.setLocation(Main.class.getResource("vue/Inscription.fxml"));
	            Parent page = loader.load();  
	            path="file:C:\\Users\\camil\\eclipse-workspace\\ppe-desktop\\PPE Labyrinth\\src\\assets/Minos.jpg";
	            primaryStage.getIcons().add(new Image(path)); 
	         
	            Scene scene = new Scene(page);
	            primaryStage.setTitle("Inscription");
	            primaryStage.initModality(Modality.APPLICATION_MODAL);
	            primaryStage.setScene(scene);
	            primaryStage.show();
	        } catch(Exception e) {
	            e.printStackTrace();}}
	 
	 @FXML
	 public void handleCompte() {
		 try {
	         	Stage primaryStage = new Stage();
	            FXMLLoader loader = new FXMLLoader();
	            loader.setLocation(Main.class.getResource("vue/Compte.fxml"));
	            Parent page = loader.load();  
	            path="file:C:\\Users\\camil\\eclipse-workspace\\ppe-desktop\\PPE Labyrinth\\src\\assets/Minos.jpg";
	            primaryStage.getIcons().add(new Image(path)); 
	         
	            Scene scene = new Scene(page);
	            primaryStage.setTitle("G�rer ses informations");
	            primaryStage.initModality(Modality.APPLICATION_MODAL);
	            primaryStage.setScene(scene);
	            CompteController Controller = loader.getController();
	            Controller.RemplirChamp();
	            primaryStage.show();
	        } catch(Exception e) {
	            e.printStackTrace();}
	 }
	 
	 @FXML public void Deconnexion() {
		 if(!ControllerSoignant.Soignants.isEmpty()) {
			 ControllerSoignant Controller = new ControllerSoignant();
			 Controller.deconnexion();
			 Invisible(false);
		 }
		 else if(!ControllerUser.Users.isEmpty()) {
			 ControllerUser Controller = new ControllerUser();
			 Controller.deconnexion();
			 Invisible(false);
		 }
	 }

	 @FXML public void SetTimer() {
		 int duree=Integer.valueOf(time.getText());
		 Timeur.SetDuree(duree);
	 }
	 
	 @FXML
	 public void GetDifficulty() {
		 String diff= this.choix.getValue().toString();
		 LabyrintheController.Diff=diff;
	 }
	 
	 @FXML
	 public void About() {
			try {
	         	Stage primaryStage = new Stage();
	            FXMLLoader loader = new FXMLLoader();
	            loader.setLocation(Main.class.getResource("vue/About.fxml"));
	            Parent page = loader.load();  
	            path="file:C:\\Users\\camil\\eclipse-workspace\\ppe-desktop\\PPE Labyrinth\\src\\assets/Minos.jpg";
	            primaryStage.getIcons().add(new Image(path)); 
	            Scene scene = new Scene(page);
	            primaryStage.setTitle("Informations");
	            primaryStage.initModality(Modality.APPLICATION_MODAL);
	            primaryStage.setScene(scene);
	            primaryStage.show();
	        } catch(Exception e) {
	            e.printStackTrace();}
	 }

	 public void Invisible(Boolean x) {
		 if(!x) {
			this.deco.setVisible(false);
			this.compte.setVisible(false);
			this.login.setVisible(true);
			this.inscription.setVisible(true);
			this.Edit.setVisible(false);
			this.inscription.setVisible(false);
			}
		 else {
			this.deco.setVisible(true);
			this.compte.setVisible(true);
			this.login.setVisible(false);
			this.inscription.setVisible(false);
			this.Edit.setVisible(false);
			if(!ControllerSoignant.Soignants.isEmpty()) {
				this.Edit.setVisible(true);
				this.inscription.setVisible(true);
			}
		 }
	 }
	 
	 
	 

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		choix.setItems(choixList);
		choix.setValue("Facile");
	}
}
