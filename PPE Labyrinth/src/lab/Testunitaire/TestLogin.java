package lab.Testunitaire;

import static org.junit.Assert.*;
import java.sql.SQLException;
import org.junit.Test;
import lab.controller.*;
import lab.classe.*;

public class TestLogin {
	
	@Test
	public void testlogin() throws SQLException {
		ControllerUser controller = new ControllerUser();
		controller.Login("zi.co", User.hash("145"));
		User patient = new User(2, "co", "Zi", "zi.co", "03c0aa74a1d4744e27e54cee53cd546331b5e0bf20b2cd7d58ebc799232f2f3a");
		assertTrue("Connexion fail", controller.Users.get(0).getClass()==(patient.getClass()));
	}

}
