package lab.vue;

import java.sql.*;
import lab.Main;
import lab.classe.User;
import lab.controller.ControllerSoignant;
import lab.controller.ControllerUser;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class LoginController  {
	@FXML
	private TextField login;
	@FXML
	private PasswordField password;
	@FXML
	private Label etat;
	@FXML
	private Button cancel;
	@FXML
	private CheckBox check;
    private String path;
	
	 @FXML
	public void handleOk(ActionEvent event) throws SQLException {
		 String Rlogin=login.getText().toString();
		 String Rpassword=User.hash(password.getText().toString());
		 if(check.selectedProperty().getValue()) {
			 ControllerSoignant Controller = new ControllerSoignant();
			 Controller.Login(Rlogin, Rpassword);
			 if(!Controller.getListe().isEmpty()) {
				 handleCancel(event);
				 Main.window(true);
				 Main.FullScreen();
			 }
			 else {
				 etat.setText("Mauvais Mot de passe / Login ou Soignant non coch�");
			 }
		 }
		 else {
			 ControllerUser Controller = new ControllerUser();
			 Controller.Login(Rlogin, Rpassword);
			 if(!Controller.getListe().isEmpty()) {
				 handleCancel(event);
				 Main.window(true);
				 Main.FullScreen();
			 }
			 else {
				 etat.setText("Mauvais Mot de passe / Login ou Soignant coch�");
			 }
		 }
	}
		 
	
	@FXML
	public void handleInscription() {
		try {
         	Stage primaryStage = new Stage();
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("vue/Inscription.fxml"));
            Parent page = loader.load();   
            path="file:C:\\Users\\camil\\eclipse-workspace\\ppe-desktop\\PPE Labyrinth\\src\\assets/Minos.jpg";
            primaryStage.getIcons().add(new Image(path)); 
            
            Scene scene = new Scene(page);
            primaryStage.setTitle("Inscription");
            primaryStage.initModality(Modality.APPLICATION_MODAL);
            primaryStage.setScene(scene);
            primaryStage.show();
        } catch(Exception e) {
            e.printStackTrace();}}
	
	
	@FXML
	public void handleCancel(ActionEvent event) {
		((Node)(event.getSource())).getScene().getWindow().hide();
		
		
	}
}

