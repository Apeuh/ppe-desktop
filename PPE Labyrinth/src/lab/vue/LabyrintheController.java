package lab.vue;

import java.io.IOException;
import java.net.URL;
import java.sql.Date;
import java.sql.SQLException;
import java.sql.Time;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventType;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import lab.Main;
import lab.classe.Partie;
import lab.controller.ControllerMaze;
import lab.controller.ControllerPartie;
import lab.controller.ControllerUser;
import lab.util.Timeur;

public class LabyrintheController {
	
	private String path;
	@FXML
	public Canvas canvasmaze;
	@FXML
	private Label timeur;
	@FXML private Button pause;
	private  static Dictionary<String,Integer> Difficulte = new Hashtable<String,Integer>();
	public static String Diff="Facile";
	ControllerMaze controller;
	@FXML
	private Label score;
	private static boolean gamestop;
	
	 @FXML
	public void Play(ActionEvent event) throws IOException, InterruptedException{
		 gamestop=false;
		 Difficulte.put("Facile", 17);
			Difficulte.put("Moyen", 27);
			Difficulte.put("Difficile", 37);
			 controller= new ControllerMaze(Difficulte.get(Diff),this.timeur);
			 if (Diff=="Facile"){
				 controller.setBlockSize(30);
			 }
			 else if (Diff=="Moyen"){
				 controller.setBlockSize(25);
			 }
			 else if (Diff=="Difficile"){
				 controller.setBlockSize(20);
			 }
				controller.execute(this.canvasmaze);
				canvasmaze.requestFocus();
				
				
				
		}
	 
	 @FXML
	 public void handleScore() {
			try {
	         	Stage primaryStage = new Stage();
	            FXMLLoader loader = new FXMLLoader();
	            loader.setLocation(Main.class.getResource("vue/Score.fxml"));
	            Parent page = loader.load();   
	            path="file:C:\\Users\\camil\\eclipse-workspace\\ppe-desktop\\PPE Labyrinth\\src\\assets/Minos.jpg";
	            primaryStage.getIcons().add(new Image(path)); 
	            ControllerPartie.resultats.clear();
	            Scene scene = new Scene(page);
	            primaryStage.setTitle("Score");
	            primaryStage.initModality(Modality.APPLICATION_MODAL);
	            primaryStage.setScene(scene);
	            primaryStage.show();
	        } catch(Exception e) {
	            e.printStackTrace();}
	 }
	 
	 @FXML
	 public void handlePause() {
		Timeur.Stop();
		gamestop=true;
	 }
	 
	 public void KeyMaze(KeyEvent k) throws IOException, SQLException {
		 if (gamestop==false) {
 		 KeyCode keyCode=k.getCode();
		 String KcdStrg=keyCode.toString();
		 controller.MoveMaze(KcdStrg);
		 }

 	}
	 public void Victoire() throws IOException, SQLException {
		  Stage ThirdStage = new Stage();
          FXMLLoader loader = new FXMLLoader();
          loader.setLocation(Main.class.getResource("vue/Victoire.fxml"));
          Parent page = loader.load();
          path="file:C:\\Users\\camil\\eclipse-workspace\\ppe-desktop\\PPE Labyrinth\\src\\assets/cup.png";
          ThirdStage.getIcons().add(new Image(path));
          Scene scene = new Scene(page);
          ThirdStage.setTitle("Victoire");
          ThirdStage.initModality(Modality.APPLICATION_MODAL);
          ThirdStage.setScene(scene);
          ThirdStage.show();
          Object control = loader.getController();
          ((LabyrintheController) control).GetScore();
          Timeur.Stop();
          
	 }
	 
	 public void GetScore() throws SQLException {
		 int timer = new Timeur().getScore();
		 String newScore=String.valueOf(timer);
         score.setText(newScore);
         java.util.Date utilDate = new java.util.Date();
         Timestamp dtf = new Timestamp(utilDate.getTime());
         Time time = new Timeur().getTime();       
         Partie scores = new Partie(ControllerUser.Users.get(0).getId(),dtf,Diff,time);
         new ControllerPartie().addResult(scores);
	 }
}
	
	

	
	
