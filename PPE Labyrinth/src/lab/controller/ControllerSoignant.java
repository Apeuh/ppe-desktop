package lab.controller;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import lab.Main;
import lab.classe.Soignant;
import lab.modele.ModeleSoignant;

public class ControllerSoignant {
	public static ArrayList<Soignant> Soignants = new ArrayList<Soignant>();

	public void Login(String login, String password) throws SQLException {
		 ModeleSoignant Modele = new ModeleSoignant();
		 ResultSet rs =Modele.Login(login, password);
		 int id=0;
		 String nom=null;
		 String prenom = null;
		 String password1=null;
		 try {
			 while(rs.next()) {					
				 id=rs.getInt("idAssistant");
		         nom=rs.getString("nom");
		         prenom=rs.getString("prenom");	
		         password1=rs.getString("password");
			}
			 if(nom!=null) {
				 Soignant soignant= new Soignant(id,nom,prenom,login,password1);
				 Soignants.add(soignant);
	}
		 }
		 catch(Exception e) {
	           e.printStackTrace();
	           }
		
	}
	
	public void Inscription(String nom, String prenom, String login, String password) throws SQLException {
		 ModeleSoignant Modele = new ModeleSoignant();
		 Modele.Inscription(nom, prenom, login, password);
	}
	
	public void Changement(String nom, String prenom, String password) throws SQLException {
		ModeleSoignant Modele = new ModeleSoignant();
		Modele.Changement(nom, prenom, password);
		Soignants.get(0).setNom(nom);
		Soignants.get(0).setPrenom(prenom);
		Soignants.get(0).setPassword(password);
	}
	
	public ArrayList<Soignant> getListe() {
		return Soignants;
	}
	
	public void deconnexion() {
		ControllerSoignant.Soignants.clear();
		Main.FullScreen();
		
	}
}
