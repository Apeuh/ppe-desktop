package lab.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class dbConnection {
	private static final String url="jdbc:mysql://localhost:3306/ppe";
	private static final String user ="root";
	private static final String password ="";
	public Connection databaseLink;
	
	public Connection connect() throws SQLException {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			databaseLink=DriverManager.getConnection(url,user,password);
		}catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
			}
		return databaseLink;
	}
	
	


}	

